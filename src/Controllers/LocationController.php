<?php

namespace Location\Czech\Controllers;

use App\CoreModule\System\Controllers\Controller;
use Location\Czech\Models\LocationManager;
use Location\Czech\Models\UpdateManager;

/**
 * Process request for location in czech republic
 */
class LocationController extends Controller
{
    function __construct(
        LocationManager $locationManager,
        UpdateManager $updateManager
    )
    {
        $this->locationManager = $locationManager;
        $this->updateManager = $updateManager;
    }

    /**
     * @CliAction
     * Update/create location
     */
    public function update()
    {
        // Big csv - 20MB
        ini_set('memory_limit', '2048M');
        $this->updateManager->download();
        $this->updateManager->insertStreetToDb();
    }

    /**
     * @ApiAction
     * Get Regions
     */
    public function getRegions()
    {
        echo json_encode($this->locationManager->getRegions());
        exit;
    }

    /**
     * @ApiAction
     * Get Cities
     */
    public function villages($region)
    {
        echo json_encode($this->locationManager->getVillages(urldecode($region)));
        exit;
    }

    /**
     * @ApiAction
     * Get streets
     */
    public function streets($locationId)
    {
        echo json_encode($this->locationManager->getStreets($locationId));
        exit;
    }

    /**
     * @ApiAction
     * Get orientation numbers
     */
    public function orientations($street, $locationId)
    {
        echo json_encode($this->locationManager->getOrientationNumbers(urldecode($street), $locationId));
        exit;
    }
}