<?php

namespace Location\Czech\Models;

use Utils\UserException;
use Utils\Csv;
use Utils\Printer;
use Utils\Utility\ConvertUtils;
use PDOException;
use DbHelper;
use FileSystemHelper;
use ZipArchive;

/**
 * Class for manage update czech location
 */
class UpdateManager
{
    /**
     * Databázový wrapper
     * @var DbHelper
     */
    public $db;

    /**
     * FileSystemHelper
     * @var FileSystemHelper
     */
    public $fileSystem;

    public $folderPath = "/public/files/CzechLocation/";

    function __construct(
        DbHelper $db,
        FileSystemHelper $fileSystem
    )
    {
        $this->db = $db;
        $this->fileSystem = $fileSystem;
    }

    /**
     * 
     * Download all required files https://vdp.cuzk.cz/vymenny_format/csv/$lastDayInLastMonth_OB_ADR_csv.zip 
     * 20200331 means - 2020|03|31 - Y.m.d => New file every month. Always last day in month - example: 2020|02|29
     * @return void
    */
    public function download()
    {
        Printer::cli("Download files");
        $lastDayInLastMonth = str_replace("-", "", date('Y-m-d',strtotime('last day of last month')));
        $fileName = $lastDayInLastMonth . "_OB_ADR_csv.zip";
        $fullPath = $this->folderPath . $fileName;
        $zip = file_get_contents("https://vdp.cuzk.cz/vymenny_format/csv/" . $fileName);
        $this->fileSystem->save($fullPath, $zip);
        $this->extractZipToSameFolder($this->fileSystem::$projectDir . $this->folderPath, $fileName);
        $this->fixCsvEncoding();
        Printer::cli("Saved and extracted + FixCsvEncoding!!!");
    }

    public function extractZipToSameFolder($destination, $fileName)
    {
        $zip = new \ZipArchive;
        if ($zip->open($destination . $fileName) === TRUE) {
            $zip->extractTo($destination);
            $zip->close();
        } else {
            throw new UserException("Extract zip fail");
        }
    }

    public function fixCsvEncoding()
    {
        Printer::cli("FixCsv encoding convert from w1250 to utf8");
        $projectDir = $this->fileSystem::$projectDir;
        $scandirPath = $this->folderPath . "CSV/";
        $absolutePath = $projectDir . $scandirPath;
        $files = $this->fileSystem->scandir($scandirPath);

        foreach ($files as $key => $fileName) {
            $csvPath = $absolutePath.$fileName;
            $file = file_get_contents($csvPath);
            $file = iconv( "Windows-1250", "UTF-8", ($file));
            file_put_contents($csvPath, $file);
        }
    }

    public function insertStreetToDb()
    {   
        $projectDir = $this->fileSystem::$projectDir;
        $scandirPath = $this->folderPath . "CSV/";
        $absolutePath = $projectDir . $scandirPath;
        $files = $this->fileSystem->scandir($scandirPath);
        
        // Recreate table
        $this->recreateTable();
       
        $countFiles = count($files);

        foreach ($files as $k => $fileName) {
            Printer::cli($fileName);
            $csvPath = $absolutePath.$fileName;
            $csv = new Csv($csvPath, ";");
            $data = [];
            $previousVillageCode = 0;

            foreach ($csv->csv as $key => $row) {
                // Skip first row - columns names
                if ($key == 0) {
                    continue;                }

                $villageCode = $row[1];
                if (!empty($row[3])) {
                    $villageCode = $row[3];
                }

                // Get location from db
                if ($villageCode != $previousVillageCode) {
                    $previousVillageCode = $villageCode;
                    $location = $this->db->queryOne("SELECT * FROM czech_location_villages WHERE  village_code = ?", [$villageCode]);
                    if (!$location) {
                        $villageName = $row[2];
                        $location = $this->db->queryOne("SELECT * FROM czech_location_villages WHERE  village = ?", [$villageName]);
                        if (!$location) {
                            var_dump($row);
                            die;
                        }
                    }
                }

                //*** Convert GPS coordinate to latitude and longtitude ***//
                $x = (float)$row[17];
                $y = (float)$row[16];
                $gpsCoordinate = $this->STJSKtoWGS84($x, $y);
                $data[] = $this->prepareData($row, $gpsCoordinate, $location['location_id']);
            }
            
            Printer::cli("Insert data about city/village:" . $location['village'] . " (". $k ."/". $countFiles .") - fileName: ". $fileName);
            
            if (count($data) == 0 ) {
                Printer::cli("Insert data about city/village:" . $location['village']);
                Printer::cli("Csv Path:" . $csvPath);
                Printer::cli("Houses number " . count($data));
                Printer::cli("Houses number " . count($data), 'warning');
            } else {
                $this->db->insertAll("czech_location_streets", $data, 1000);
                unset($data);
            }
        }
    }

    /**
     * From S-JTSK into WGS84 for coordinate from czech republic
     * @param float $x X coordinate
     * @param float $y Y coordinate
     */
    public function STJSKtoWGS84(float $x, float $y)
    {
        // Set precision for float number to first 30 numbers
        ini_set("precision", 30);
        $referenceY = 703000;
        $referenceX = 1058000;
        $koeficients = [
            "A" => [
                5 => 0.001325132993, // 1.33E-03
                6 => -0.000008916429099, // -8.9E-06
                7 => -0.000001156917384, // -1.16E-06
                8 => -0.0000000000000229875025, // -2.30E-14
                9 => 0.0000000000002087176527, // 2.09E-13
                10 => -0.0000000000008219794748, // -8.22E-13
                11 => 0.00000000000000000002191874854, // 2.191874854E-20
                12 => 0.000000000000000000005305545189, // 5.305545189E-21
                13 => 0.0000000000000000001760134043, // 1.760134043E-19
                14 => 0.000000000000000000006270628603, // 6.270628603E-21
            ],
            "B" => [
                5 => -0.0001019442857,
                6 => 0.000001794902692, //1.794902692E-06,
                7 => -0.00001383338939, //-1.383338939E-05
                8 => -0.0000000000003294257309, //-3.294257309E-13
                9 => 0.000000000002506009659, // 2.506009659E-12
                10 => 0.0000000000003291143794, // 3.291143794E-13
                11 => 0.00000000000000000004567560092, // 4.567560092E-20
                12 => -0.0000000000000000004843979237, // -4.843979237E-019
                13 => -0.0000000000000000001182561606, // -1.182561606E-019
                14 => 0.0000000000000000001641107774, // 1.641107774E-19
            ],
        ];

        $G = $x-$referenceX; //1058000
        $H = $y-$referenceY; 

        $I = 50 + $koeficients["A"][5] + 
            ($koeficients["A"][6] * $G) +
            ($koeficients["A"][7] * $H) + 
            ($koeficients["A"][8] * pow($G, 2)) +
            ($koeficients["A"][9] * $H * $G) + 
            ($koeficients["A"][10] * pow($H, 2)) + 
            ($koeficients["A"][11] * pow($G, 3)) + 
            ($koeficients["A"][12] * pow($G, 2) * $H) + 
            ($koeficients["A"][13] * $G * pow($H, 2)) + 
            ($koeficients["A"][14] * pow($H, 3));

        $J = 15 + $koeficients["B"][5] + 
            ($koeficients["B"][6] * $G) +
            ($koeficients["B"][7] * $H) + 
            ($koeficients["B"][8] * pow($G, 2)) +
            ($koeficients["B"][9] * $G * $H) + 
            ($koeficients["B"][10] * pow($H, 2)) + 
            ($koeficients["B"][11] * pow($G, 3)) + 
            ($koeficients["B"][12] * pow($G, 2) * $H) + 
            ($koeficients["B"][13] * $G * pow($H, 2)) + 
            ($koeficients["B"][14] * pow($H, 3));

        return ["longitude" => $I, "latitude" => $J];
    }

    public function recreateTable()
    {
        Printer::cli("Recreate table czech_location_streets table");
        $deleteTableSql = 'DROP TABLE IF EXISTS `czech_location_streets`;';
        $creatTableSql = 'CREATE TABLE `czech_location_streets` (
                  `id` int(11) NOT NULL AUTO_INCREMENT,
                  `location` int(11) NOT NULL,
                  `street` varchar(255) COLLATE utf8_bin NOT NULL,
                  `orientation_number` varchar(5) COLLATE utf8_bin NOT NULL,
                  `house_number` int(11) NOT NULL,
                  `longitude` float NOT NULL,
                  `latitude` float NOT NULL,
                  PRIMARY KEY (`id`),
                  KEY `location_id` (`location`),
                  CONSTRAINT `czech_location_streets_ibfk_1` FOREIGN KEY (`location`) REFERENCES `czech_location_villages` (`location_id`)
                ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;';

        $this->db->query($deleteTableSql);
        $this->db->query($creatTableSql);
    }

    public function prepareData($row, $gpsCoordinate, $locationId)
    {
        $houseNumber = $row[12]; //Číslo domovní
        $orientationNumber = $row[13]; //Číslo orientační
        $street = $row[10]; // Ulice

        // Control - Znak čísla orientačního - example: 2a, 2b ...
        if ($row[14]) {
            $orientationNumber = $orientationNumber . $row[14];
        }

        if (!$orientationNumber) {
            $orientationNumber = $houseNumber;
        }

        // If dont exist street - village without street
        if (!$street) {
            $street = "Bez ulice";
        }

        return [
            "location" => $locationId,
            "street" => $street, //Název ulice
            "house_number" => $houseNumber, //Číslo domovní
            "orientation_number" => $orientationNumber, //Číslo orientační
            "longitude" => $gpsCoordinate['longitude'],
            "latitude" => $gpsCoordinate['latitude'],
        ];
    }
}