<?php

namespace Location\Czech\Models;

use Utils\UserException;
use Utils\Csv;
use PDOException;
use DbHelper;

/**
 * Class for manage czech location
 */
class LocationManager
{
    /**
     * Databázový wrapper
     * @var DbHelper
     */
    public $db;

    function __construct(
        DbHelper $db
    )
    {
        $this->db = $db;
    }

    public function getRegions($edit = false)
    {
        $res = $this->db->queryAll("SELECT distinct(region) FROM czech_location_villages", [], \PDO::FETCH_COLUMN);
        if ($edit) {
            return array_combine($res, $res);
        }
        return array_merge(["" => 0], (array_combine($res, $res)));//Vyberte region
    }

    public function getVillages($region, $edit = false)
    {
        $res = $this->db->queryAll("SELECT distinct(village), location_id, poradi FROM czech_location_villages WHERE region = ? ORDER BY poradi DESC", [$region]); //Musí být podle pořadí DESC vždy!
        if (empty($res)) {
            return;
        }

        $villages = []; //JSON

        if (!$edit) {
            $villages[] = "";//Vyber město/obec - prázdno
        }

        foreach ($res as $key => $val) {
            //Vybere podle pořáadí a seřadí
            if($val['poradi'] == "2" OR $val['poradi'] == "3" OR $val['poradi'] == "4" OR $val['poradi'] == "5" OR $val['poradi'] == "6" OR $val['poradi'] == "7" OR $val['poradi'] == "8"){
                $villages[' '.$val['location_id']] = '- '.$val['village'].' -';  //vypíše město z pořadí a přidá mu ID
            }else{
                $villages[' '.$val['location_id']] = $val['village']; // pokud není v pořadí vypíše pod něj
            }

            if($val['village'] == "----------------------"){
                $villages[$val['location_id']] = $val['village'];    //oddělovač
            }
        }

        return $villages;
    }

    public function getStreets($locationId, $edit = false)
    {
        $res = $this->db->queryAll("SELECT distinct(street) FROM czech_location_streets WHERE location = ? ORDER BY street ASC", [ $locationId ], \PDO::FETCH_COLUMN);
        if ($edit) {
            return array_combine($res, $res);
        }
        return array_merge([0 => ""], (array_combine($res, $res)));   //Vyberte ulici
    }

    public function getOrientationNumbers($street, $locationId, $edit = false)
    {
        $res = $this->db->queryAll("SELECT orientation_number FROM czech_location_streets WHERE street = ? AND location = ?", [$street, $locationId], \PDO::FETCH_COLUMN);
        $combine = array_combine($res, $res);
        asort($combine);

        if ($edit) {
            return $combine;
        }
        return array_merge([0 => ""], $combine);//Vyberte číslo orientační
    }

    public function getStreet($locationId, $street)
    {
        $res = $this->db->queryOne("SELECT * FROM czech_location_streets WHERE location = ? AND street = ?", [$locationId, $street]);
        if ($res) {
            return $res;
        }
        return false;
    }

    public function getLocationId($village)
    {
        $res = $this->db->queryOne("SELECT * FROM czech_location_villages WHERE village = ?", [$village], \PDO::FETCH_COLUMN);

        if ($res) {
            return $res;
        }
        return false;
    }
}