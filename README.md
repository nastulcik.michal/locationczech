# LocationCzech

Module with api for easy working with all locations in czech republic + download and update streets in cities

Require:
1) My MVC Framework,
2) cyborg/utils module

Install instructions:

1) composer require location/czech:dev-master
2) Insert mysql dump - table with all cities and villages in czech republic - file name is czech_location_villages 
3) Last part is run cli update in public folder - php index.php Modules-CzechLocation-Location/update